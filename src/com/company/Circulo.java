package com.company;

public class Circulo extends FormaGeometrica{

    private double raio;

    public double calcularArea(double raio) {

        return Math.pow(raio,2) * Math.PI;
    }

    @Override
    public double calcularArea() {
        return 0;
    }
}

