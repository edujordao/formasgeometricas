package com.company;

public abstract class FormaGeometrica {

    private int quantidadeLados;
    private double area;

    public int getQuantidadeLados() {
        return quantidadeLados;
    }

    public void setQuantidadeLados(int quantidadeLados) {
        this.quantidadeLados = quantidadeLados;
    }

    public double getArea() {
        return area;
    }

    public void setArea(double area) {
        this.area = area;
    }

    public abstract double calcularArea();
}
