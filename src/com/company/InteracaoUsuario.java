package com.company;

import javax.swing.*;
import java.util.ArrayList;

public class InteracaoUsuario {

    IO io = new IO();
    ArrayList<Double> medidas = new ArrayList<>();

    public void receberQuantidadeLados() {

        boolean sair = false;

        do {

            double medida = 0;

            while (medida != -1) {
                medida = io.solicitarInformacao("Entre com as medidas da forma geométrica desejada, quando terminar digite -1 ou -2 para encerrar:");

                if (medida != -1 && medida != -2) {
                    medidas.add(medida);
                }
                else if(medida == -2) {
                    sair = true;
                    return;
                }
            }

            if(!sair)
                descobrirForma(medidas.size());
        }
        while(!sair);
    }

    public void descobrirForma(double quantidadeLados){

        switch ((int) quantidadeLados){

            case 1: {
                Circulo circulo = new Circulo();
                io.exibirInformacao("A área do círculo é " + circulo.calcularArea(medidas.get(0)));
                return;
            }
            case 2: {
                Retangulo retangulo = new Retangulo();
                io.exibirInformacao("A área do retângulo é " + retangulo.calcularArea(medidas.get(0), medidas.get(1)));
                return;
            }
            case 3:{
                Triangulo triangulo = new Triangulo();
                if(triangulo.validaTriangulo(medidas.get(0),medidas.get(1),medidas.get(2)))
                    io.exibirInformacao("A área do triângulo é " + triangulo.calcularArea(medidas.get(0),medidas.get(1),medidas.get(2)));
                else
                    io.exibirInformacao("As medidas do triângulo são inválidas");
                return;
            }

            default: io.exibirInformacao("Forma geométrica não reconhecida");

        }
    }
}
