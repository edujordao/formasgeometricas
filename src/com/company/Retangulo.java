package com.company;

public class Retangulo extends FormaGeometrica {

    private double altura;
    private double largura;

    public double calcularArea(double altura, double largura) {

        return altura * largura;
    }

    @Override
    public double calcularArea() {
        return 0;
    }
}
