package com.company;

import javax.swing.*;

public class Triangulo extends FormaGeometrica{

    private double ladoA, ladoB, ladoC;

    public double calcularArea(double ladoA, double ladoB, double ladoC) {

        double s = (ladoA + ladoB + ladoC) / 2;
        return Math.sqrt(s * (s - ladoA) * (s - ladoB) * (s - ladoC));
    }

    @Override
    public double calcularArea() {
        return 0;
    }

    public boolean validaTriangulo(double ladoA, double ladoB, double ladoC){

        if((ladoA + ladoB) > ladoC && (ladoA + ladoC) > ladoB && (ladoB + ladoC) > ladoA)
            return true;
        else
            return false;
    }
}
